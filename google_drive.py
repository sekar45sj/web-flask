import io
import os
import flask

from googleapiclient.http import MediaIoBaseDownload, MediaIoBaseUpload
import googleapiclient.discovery
from google_auth import build_credentials

from googleapiclient.discovery import build
from oauth2client.service_account import ServiceAccountCredentials

app = flask.Blueprint('google_drive', __name__)

def build_drive_api_v3():
    credentials = build_credentials()
    return googleapiclient.discovery.build('drive', 'v3', credentials=credentials)


def upload_file_to_drive(fh, name, parent_id):
  drive_api = build_drive_api_v3().files()

  file_metadata = { 'name': name, 'mimeType': '*/*', 'parents': [parent_id] }
  media = MediaIoBaseUpload(fh, mimetype='*/*', resumable=True)
  return drive_api.create(body=file_metadata, media_body=media, fields='id', supportsAllDrives=True).execute()


def upload_folder_to_drive(name, parent_id):
  drive_api = build_drive_api_v3().files()
  
  body = {
    'name': name,
    'mimeType': "application/vnd.google-apps.folder", 
    'parents': [parent_id]
  }
  return drive_api.create(body = body, supportsAllDrives=True).execute() 



@app.route('/gdrive/view/<file_id>', methods=['GET'])
def view_file(file_id):
    drive_api = build_drive_api_v3().files()

    metadata = drive_api.get(fields = "name,mimeType", fileId = file_id).execute()

    request = drive_api.get_media(fileId=file_id)
    fh = io.BytesIO()
    downloader = MediaIoBaseDownload(fh, request)

    done = False
    while done is False:
        status, done = downloader.next_chunk()

    fh.seek(0)

    return flask.send_file(
        fh, attachment_filename = metadata['name'],
        mimetype = metadata['mimeType']
    )


SERVICE_ACCOUNT_FILE_PATH = os.environ.get("DOMAIN_WIDE_DELEGATE_CREDENTIAL", default="")
DOMAIN_WIDE_DELEGATION_SCOPES = ["https://www.googleapis.com/auth/drive", "https://www.googleapis.com/auth/drive.file", "https://www.googleapis.com/auth/admin.directory.user.readonly"]

# def get_drive_api_service(user_email):
  
#   credentials = ServiceAccountCredentials.from_json_keyfile_name(
#       SERVICE_ACCOUNT_FILE_PATH,
#       scopes = DOMAIN_WIDE_DELEGATION_SCOPES)
  
#   credentials = credentials.create_delegated(user_email)
  
#   return build("drive", "v3", credentials=credentials)


def get_directory_api_service(user_email):
  
  credentials = ServiceAccountCredentials.from_json_keyfile_name(
      SERVICE_ACCOUNT_FILE_PATH,
      scopes = DOMAIN_WIDE_DELEGATION_SCOPES)
  
  credentials = credentials.create_delegated(user_email)
  
  return build('admin', 'directory_v1', credentials=credentials)

def build_directory_api_v1():
    credentials = build_credentials()
    return googleapiclient.discovery.build('admin', 'directory_v1', credentials=credentials)