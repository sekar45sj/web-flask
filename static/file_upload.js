
function gotoHome() {
    window.location.href = '/'
}

var pageTokens = [];

$(document).ready(function(){

    drivePreviousParentIds = ['root']
    driveParentId = 'root'

    $("#close_drive_button").click(function() {
        drive_file_div = document.getElementById('drive_file_div')
        drive_file_div.innerHTML = ''
        checkDriveDivVisibility()
    })

    // $("#back_drive_div").click(function() {
    //     $.ajax({url: "/parent/" + drivePreviousParentId, success: function(result) {
    //         if(result['data'] == 'NO_LOGIN') {
    //             window.location .href = 'google/login'
    //         } else {
    //             console.log(result)
    //             setDriveDialog(result['data'])
    //             checkDriveDivVisibility()
    //         }
    //     }});
    // })

    $("#drive_file").click(function() {
        fetchFilesFromDrive(null)
    })

    $("#next_page_drive_button").click(function() {
        nextPageToken = pageTokens[pageTokens.length - 1]
        fetchFilesFromDrive(nextPageToken)
    })

    $("#prev_page_drive_button").click(function() {
        pageTokens.pop()
        pageTokens.pop()
        if(pageTokens.length > 0) {
            prevPageToken = pageTokens[pageTokens.length - 1]
        } else {
            prevPageToken = null
        }
        fetchFilesFromDrive(prevPageToken)
    })

    $("#logout").click(function() {
        window.location.href = 'google/logout'
    })
});


function fetchFilesFromDrive(pageToken) {
    $.ajax({url: "/parent/all", data: {'pageToken': pageToken}, success: function(result) {
        if(result['data'] == 'NO_LOGIN') {
            window.location .href = 'google/login'
        } else {
            console.log(result)
            if('nextPageToken' in result['data']) {
                nextPageToken = result['data']['nextPageToken']
                pageTokens.push(nextPageToken)
                document.getElementById('next_page_drive_button').disabled = false
            } else {
                pageTokens.push(null)
                document.getElementById('next_page_drive_button').disabled = true
            }
            if(pageTokens.length > 1) {
                document.getElementById('prev_page_drive_button').disabled = false
            } else {
                document.getElementById('prev_page_drive_button').disabled = true
            }
            console.log(pageTokens)
            setDriveDialog(result['data']['files'])
            checkDriveDivVisibility()
        }
    }});    
}

function setDriveDialog(fileList) {
    drive_file_div = document.getElementById('drive_file_div')
    drive_file_div.innerHTML = ''
    for(file in fileList) {
        div = document.createElement('div');
        div.id = fileList[file]['id'] + '_div'
        div.classList.add('drive_file') 

        anchor = document.createElement('a');
        anchor.id = fileList[file]['id']
        anchor.name = fileList[file]['name']
        anchor.href = '#'
        anchor.innerHTML = fileList[file]['name']
        anchor.setAttribute('mimeType', fileList[file]['mimeType'])
        anchor.setAttribute('onClick', 'clickDriveFile(this)')

        // image = document.createElement('img');
        // image.id = fileList[file]['id']
        // image.name = fileList[file]['name']
        // image.src = fileList[file]['iconLink']
        // image.setAttribute('mimeType', fileList[file]['mimeType'])
        // image.setAttribute('onClick', 'clickDriveFile(this)')

        // div.appendChild(image)
        div.appendChild(anchor)
        drive_file_div.appendChild(div)
    }
}

function checkDriveDivVisibility() {
    drive_div = document.getElementById('drive_div')
    drive_file_div = document.getElementById('drive_file_div')
    if(drive_file_div.hasChildNodes()) {
        drive_div.style.visibility = 'visible'
    } else {
        drive_div.style.visibility = 'hidden'
    }
}

function clickDriveFile(element) {
    console.log(element['id'])
    // if(element.getAttribute('mimeType') == 'application/vnd.google-apps.folder') {
    //     $.ajax({url: "/parent/all", success: function(result) {
    //         drivePreviousParentId = driveParentId
    //         driveParentId = element['id']
    //         console.log(result)
    //         setDriveDialog(result['data'])
    //         checkDriveDivVisibility()
    //     }});
    // } else {
    //     console.log(element['id'])
    // }
}