
function gotoHome() {
    window.location.href = '/'
}

var pageTokens = [];

$(document).ready(function(){

    drivePreviousParentIds = ['root']
    driveParentId = 'root'

    $("#close_drive_button").click(function() {
        drive_file_div = document.getElementById('drive_file_div')
        drive_file_div.innerHTML = null
        document.getElementById('drive_search_text').value = ''
        drive_div.style.visibility = 'hidden'
        // checkDriveDivVisibility()
    })

    $("#local_file_button").click(function() {
        document.getElementById('local_file').click()
    })
    
    $("#local_file").change(function() {
        local_file = document.getElementById('local_file')
        files = local_file.files
        if(files.length > 0) {
            addFile(files[0])
        }
        document.getElementById('drive_file_id').value = null
        document.getElementById('file_name_text').value = null
        document.getElementById('file_link_text').value = null
        document.getElementById('file_link_div').style.display = 'none'
    })
    

    // $("#back_drive_div").click(function() {
    //     $.ajax({url: "/parent/" + drivePreviousParentId, success: function(result) {
    //         if(result['data'] == 'NO_LOGIN') {
    //             window.location .href = 'google/login'
    //         } else {
    //             console.log(result)
    //             setDriveDialog(result['data'])
    //             checkDriveDivVisibility()
    //         }
    //     }});
    // })

    $("#drive_file").click(function() {
      pageTokens = []
      fetchFilesFromDrive(null)
    })

    $("#drive_search_button").click(function() {
      pageTokens = []
      fetchFilesFromDrive(null)
    })
    
    $("#file_link").click(function() {
        file_link_div = document.getElementById('file_link_div')
        if(file_link_div.style.display == 'none') {
            file_link_div.style.display = 'block'
        } else {
            file_link_div.style.display = 'none'
        }
        removeFileContainer()
    })

    $("#next_page_drive_button").click(function() {
        nextPageToken = pageTokens[pageTokens.length - 1]
        fetchFilesFromDrive(nextPageToken)
    })

    $("#prev_page_drive_button").click(function() {
        pageTokens.pop()
        pageTokens.pop()
        if(pageTokens.length > 0) {
            prevPageToken = pageTokens[pageTokens.length - 1]
        } else {
            prevPageToken = null
        }
        fetchFilesFromDrive(prevPageToken)
    })

    $("#logout").click(function() {
        window.location.href = 'google/logout'
    })

    // $('#secondary_contact_select').change(function() {
    //   secondary_contact_select = document.getElementById('secondary_contact_select')
    //   secondary_contact_tag = document.getElementById('secondary_contact_tag')

    //   // #label = document.createElement('label');
    //   // #label.id = 'added_file_name'
    //   // #label.innerText = file['name']
    //   // #secondary_contact_tag.appendChild()
    // })

    $("#secondary_contact_select").on('change', function () {
      email = $(this).val()
      option = document.getElementById(email + '_option')
      fullName = option.label

      if(email != 'empty') {
        addContactToTagDiv(fullName, email)
        option.remove()
        $(this).context.value = ''
      }
    })

    $(window).keydown(function(event){
      if( (event.keyCode == 13)) {
        event.preventDefault();
      }
    });

});

function loadFunction() {
  $(".contacts_tag").click(function() {
    button = $(this).context
    email = button.getAttribute('email')
    fullName = button.getAttribute('fullName')
    label = document.getElementById(email + '_label')
    label.remove()
    addContactToSelect(fullName, email)
  })
}

function getSecondaryContacts() {
  secondary_contact = document.getElementById('secondary_contact')
  secondary_contact_tag = document.getElementById('secondary_contact_tag')
  contacts = []
  secondary_contact_tags= secondary_contact_tag.children
  for(i = 0; i < secondary_contact_tags.length; i++) {
    contacts.push(secondary_contact_tags[i].getAttribute('email'))
  }
  contactString = contacts.join(',')
  secondary_contact.value = contactString
}

function addContactToTagDiv(fullName, email) {
  secondary_contact_tag = document.getElementById('secondary_contact_tag')
  
  button = document.createElement('input');
  button.id = email + '_button'
  button.type = 'button'
  button.value = 'x'
  button.classList = ['contacts_tag']
  button.setAttribute('email', email)
  button.setAttribute('fullName', fullName)

  label = document.createElement('label');
  label.id = email + '_label'
  label.setAttribute('email', email)
  label.setAttribute('fullName', fullName)

  label.appendChild(button)
  label.innerHTML = fullName + ' ' + label.innerHTML

  secondary_contact_tag.appendChild(label)

  loadFunction()
  getSecondaryContacts()
}

function addContactToSelect(fullName, email) {
  secondary_contact_list = document.getElementById('secondary_contact_list')
  
  option = document.createElement('option');
  option.id = email + '_option'
  option.value = email
  option.innerText = fullName

  secondary_contact_list.appendChild(option)
  getSecondaryContacts()
}


function addFile(file) {
    file_container_div = document.getElementById('file_container_div')
    file_container_div.style.display = 'block'
    file_container_div.innerHTML = null

    div = document.createElement('div');
    div.id = 'added_file_div'
    div.classList.add('added_file') 

    label = document.createElement('label');
    label.id = 'added_file_name'
    label.innerText = file['name']

    input = document.createElement('input');
    input.id = 'added_file_remove_button'
    input.type = 'button'
    input.value = 'Remove'
    input.setAttribute('onClick', 'removeFileContainer()')

    div.appendChild(label)
    div.appendChild(input)
    file_container_div.appendChild(div)
}


function removeFileContainer() {
    file_container_div = document.getElementById('file_container_div')
    file_container_div.style.display = 'none'
    file_container_div.innerHTML = null
    document.getElementById('local_file').value = null
    document.getElementById('drive_file_id').value = null
}

function fetchFilesFromDrive(pageToken) {
    driveSearchText = document.getElementById('drive_search_text').value
    $.ajax({url: "/parent/all", data: {'pageToken': pageToken, 'driveSearchText': driveSearchText}, success: function(result) {
        if(result['data'] == 'NO_LOGIN') {
            window.location.href = 'google/login'
        } else {
            console.log(result)
            if('nextPageToken' in result['data']) {
                nextPageToken = result['data']['nextPageToken']
                pageTokens.push(nextPageToken)
                document.getElementById('next_page_drive_button').disabled = false
            } else {
                pageTokens.push(null)
                document.getElementById('next_page_drive_button').disabled = true
            }
            if(pageTokens.length > 1) {
                document.getElementById('prev_page_drive_button').disabled = false
            } else {
                document.getElementById('prev_page_drive_button').disabled = true
            }
            console.log(pageTokens)
            if(result['data']['files'].length == 0) {
              document.getElementById('no_file_present_div').style.display = 'block'
              document.getElementById('next_page_drive_button').disabled = true
              document.getElementById('prev_page_drive_button').disabled = true
            } else {
              document.getElementById('no_file_present_div').style.display = 'none'
            }
            setDriveDialog(result['data']['files'])
            drive_div.style.visibility = 'visible'
        }
    }});    
}

function setDriveDialog(fileList) {
    drive_file_div = document.getElementById('drive_file_div')
    drive_file_div.innerHTML = null
    for(file in fileList) {
        div = document.createElement('div');
        div.id = fileList[file]['id'] + '_div'
        div.classList.add('drive_file') 

        anchor = document.createElement('a');
        anchor.id = fileList[file]['id']
        anchor.name = fileList[file]['name']
        anchor.href = '#'
        anchor.innerHTML = fileList[file]['name']
        anchor.setAttribute('mimeType', fileList[file]['mimeType'])
        anchor.setAttribute('onClick', 'clickDriveFile(this)')

        // image = document.createElement('img');
        // image.id = fileList[file]['id']
        // image.name = fileList[file]['name']
        // image.src = fileList[file]['iconLink']
        // image.setAttribute('mimeType', fileList[file]['mimeType'])
        // image.setAttribute('onClick', 'clickDriveFile(this)')

        // div.appendChild(image)
        div.appendChild(anchor)
        drive_file_div.appendChild(div)
    }
}

function checkDriveDivVisibility() {
    drive_div = document.getElementById('drive_div')
    drive_file_div = document.getElementById('drive_file_div')
    if(drive_file_div.hasChildNodes()) {
        drive_div.style.visibility = 'visible'
    } else {
        drive_div.style.visibility = 'hidden'
    }
}

function clickDriveFile(element) {
    document.getElementById('drive_file_id').value = element['id']
    document.getElementById('local_file').value = null
    document.getElementById('file_name_text').value = null
    document.getElementById('file_link_text').value = null
    document.getElementById('file_link_div').style.display = 'none'
    document.getElementById('close_drive_button').click()
    addFile(element)
    // if(element.getAttribute('mimeType') == 'application/vnd.google-apps.folder') {
    //     $.ajax({url: "/parent/all", success: function(result) {
    //         drivePreviousParentId = driveParentId
    //         driveParentId = element['id']
    //         console.log(result)
    //         setDriveDialog(result['data'])
    //         checkDriveDivVisibility()
    //     }});
    // } else {
    //     console.log(element['id'])
    // }
}