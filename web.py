import os
import io
import flask
import google_auth
import google_drive
import uuid
import firebase_admin

from google.cloud import storage
from werkzeug.utils import secure_filename
from googleapiclient.http import MediaIoBaseDownload, MediaIoBaseUpload

from firebase_admin import credentials
from firebase_admin import firestore
from flask import Flask, render_template, request, jsonify

app = Flask(__name__)
app.secret_key = os.environ.get("FN_FLASK_SECRET_KEY", default=False)

app.register_blueprint(google_auth.app)
app.register_blueprint(google_drive.app)

firebase_admin.initialize_app()
db = firestore.client()

VIEW_URI = os.environ.get("FN_VIEW_URI", default='')
LOGIN_URI = os.environ.get("FN_LOGIN_URI", default='')
BASE_URI = os.environ.get("FN_BASE_URI", default='')
COLLECTION_NAME = os.environ.get("COLLECTION_NAME", default='')
BUCKET_NAME = os.environ.get("BUCKET_NAME", default='')
ADMIN_USER_EMAIL = os.environ.get("FN_ADMIN_USER_EMAIL", default='')
DOMAIN_NAME = os.environ.get("FN_DOMAIN_NAME", default='')
PARENT_DRIVE_ID = os.environ.get("FN_PARENT_DRIVE_ID", default='')
collection = db.collection(COLLECTION_NAME)


@app.route('/parent/<parent_id>')
def drive(parent_id):
    if google_auth.is_logged_in():
        drive_api = google_drive.build_drive_api_v3().files()
        pageToken = request.args.get('pageToken')
        driveSearchText = request.args.get('driveSearchText')
        
        drive_fields = "nextPageToken, files(id,name,mimeType,createdTime,modifiedTime,shared,webContentLink)"
        query = 'trashed = false and mimeType != "application/vnd.google-apps.folder" and name contains "' + driveSearchText + '"'
        query = query if parent_id == 'all' else query + ' and parents in "{}"'.format(parent_id)
        if pageToken == None: 
          items = drive_api.list(pageSize=100, orderBy="name", q=query, fields=drive_fields).execute()
        else:
          items = drive_api.list(pageToken = pageToken, pageSize=100, orderBy="name", q=query, fields=drive_fields).execute()
        
        # items['files'] = list(filter(lambda x: driveSearchText in x['name'], items['files']))

        return jsonify({
          'data': items
        })

    return jsonify({
      'data': 'NO_LOGIN'
    })

    

@app.route('/')
def index():
  data = {}
  if google_auth.is_logged_in():
    data['login'] = 'success'
  else:
    data['login'] = 'no_login'
  return render_template('index.html', data = data)

@app.route('/newRequest')
def newRequest():
  data = {}
  if google_auth.is_logged_in():
    data['login'] = 'success'
  else:
    data['login'] = 'no_login'
    return flask.redirect(BASE_URI, code=302)
  return render_template('newRequest.html', data = data)

@app.route('/architecture')
def architecture():
  data = {}
  if google_auth.is_logged_in():
    data['login'] = 'success'

    profile = google_auth.get_user_info()
    data['profile'] = { 'email': profile['email'], 'name': profile['name'] }
    print("profile => " + str(profile))

    users = getDirectoryUsers(profile['email'])    
    
    data['users'] = users
  else:
    data['login'] = 'no_login'
    return flask.redirect(BASE_URI, code=302)
  return render_template('architecture.html', data = data)


def getDirectoryUsers(currentUserEmail):
  users = []
  directory_api = google_drive.get_directory_api_service(ADMIN_USER_EMAIL)
  nextPageToken = None

  while True:
    if nextPageToken == None:
      directoryResult = directory_api.users().list(domain=DOMAIN_NAME, maxResults=500).execute()
    else:
      directoryResult = directory_api.users().list(domain=DOMAIN_NAME, maxResults=500, pageToken=nextPageToken).execute()

    for user in directoryResult['users']:
      print("user => " + str(user))
      if user['primaryEmail'] != currentUserEmail:
        users.append({ 'email': user['primaryEmail'], 'name': user['name']['fullName'] })
    
    if not 'nextPageToken' in directoryResult:
      break
    else:
      nextPageToken = directoryResult['nextPageToken']
  
  return users

@app.route('/view')
def view():

  data_list = []
  for doc in collection.order_by("__name__").stream():
    doc = doc.to_dict()
    data = {}
    data['name'] = doc['name']
    data['summary'] = doc['summary']
    data['file name'] = doc['file_name'] if 'file_name' in doc else ''
    data['file link'] = doc['file_link'] if 'file_link' in doc else ''
    data['file id'] = doc['file_id'] if 'file_id' in doc else ''
    data['primary contact'] = doc['primary_contact'] if 'primary_contact' in doc else ''
    data['secondary contact'] = doc['secondary_contact'] if 'secondary_contact' in doc else ''
    data_list.append(data)
  
  data = {}
  if google_auth.is_logged_in():
    data['login'] = 'success'
  else:
    data['login'] = 'no_login'
    return flask.redirect(BASE_URI, code=302)
  return render_template('view.html', result = data_list, data = data)


@app.route('/submit', methods = ['POST'])
def submit():

  print(request)  
  data = {}
  data['name'] = request.form['name']
  data['summary'] = request.form['summary']
  data['primary_contact'] = request.form['primary_contact']
  data['secondary_contact'] = request.form['secondary_contact']
  
  file = request.files['local_file']
  drive_file_id = request.form['drive_file_id']
  file_name = request.form['file_name_text']
  file_link = request.form['file_link_text']
  
  drive_api = google_drive.build_drive_api_v3().files()
  
  if file:
    print('processing local file')

    fh = io.BytesIO(file.read())

    root_folder = google_drive.upload_folder_to_drive(data['name'], PARENT_DRIVE_ID)
    driveFile = google_drive.upload_file_to_drive(fh, file.filename, root_folder['id'])

    data['file_name'] = file.filename
    data['file_id'] = driveFile.get('id')

  elif drive_file_id:
    print('processing drive file')

    metadata = drive_api.get(fields = "name,mimeType", fileId = drive_file_id).execute()
    
    if metadata['mimeType'] == 'application/vnd.google-apps.drawing':
#      extension = 'jpg'
      content_type = 'image/jpeg'
      res = drive_api.export_media(fileId = drive_file_id, mimeType = content_type)
    elif metadata['mimeType'] == 'application/vnd.google-apps.spreadsheet':
#      extension = 'xlsx'
      content_type = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
      res = drive_api.export_media(fileId = drive_file_id, mimeType = content_type)
    elif metadata['mimeType'] == 'application/vnd.google-apps.document':
#      extension = 'docx'
      content_type = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
      res = drive_api.export_media(fileId = drive_file_id, mimeType = content_type)
    else:
#      extension = metadata['name'].rsplit('.', 1)[1]
      content_type = metadata['mimeType']
      res = drive_api.get_media(fileId = drive_file_id)
    
    fh = io.BytesIO()
    downloader = MediaIoBaseDownload(fh, res)

    done = False
    while done is False:
        status, done = downloader.next_chunk()
    fh.seek(0)

    root_folder = google_drive.upload_folder_to_drive(data['name'], PARENT_DRIVE_ID)
    driveFile = google_drive.upload_file_to_drive(fh, metadata['name'], root_folder['id'])

    data['file_name'] = metadata['name']
    data['file_id'] = driveFile.get('id')

  elif file_name and file_link:
    print('processing file link')

    data['file_name'] = file_name
    data['file_link'] = file_link
  else:
    print('no file uploaded')

  print(data)
  collection.document(data['name']).set(data)
  
  return flask.redirect(VIEW_URI, code=302)


if __name__ == '__main__':
  context = ('ssl/certificate.crt', 'ssl/private.key')
#  app.run(debug = True, host = '0.0.0.0', port = 443, ssl_context=context)
  app.run(debug = True, host='0.0.0.0', port=80)
