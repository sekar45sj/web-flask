FROM python:3.8-slim

MAINTAINER Sowmith

COPY . /opt/

SHELL ["/bin/bash", "-c"]

RUN apt update && apt install vim curl wget net-tools telnet -y

RUN pip3 install -r /opt/web-flask/requirements.txt

ENTRYPOINT ["/bin/bash", "-c", "source /opt/web-flask/env.sh && python3 /opt/web-flask/web.py"]
